from django.conf.urls import include, url
from django.contrib import admin

from psdcalc.views import home, frontend
from user_auth.views import Login, logout_view

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^frontend/$', frontend, name='frontend'),
    url(r'^psdcalc/', include('psdcalc.urls')),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', logout_view, name='logout'),
]
