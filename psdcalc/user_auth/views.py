from django.views.generic import View
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout


# This view logs out a user and redirects them to the login page.
def logout_view(request):
    logout(request)
    return redirect('login')


# This view authenticates and logs in a user, and redirects them to the PSD calculator
# home page if the credentials are valid.
class Login(View):

    def get(self, request, *args, **kwargs):
        return render(request, template_name="login.html")

    def post(self, request, *args, **kwargs):

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        context = {}

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('home')
            else:
                context = {'error_msg': 'Account is inactive.'}
        else:
            context = {'error_msg': 'Invalid username and/or password.'}

        return render(request, 'login.html', context)
