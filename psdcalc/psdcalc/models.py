from django.db import models
from django.conf import settings


# This model contains the metadata for each image that a user uploads into the application
class Image(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    filename = models.CharField(max_length=100)
    amzn_filename = models.CharField(max_length=200, unique=True)
    mime_type = models.CharField(max_length=30)
    url = models.CharField(max_length=200, unique=True)
    thumbnail = models.CharField(max_length=200)
    psd = models.CharField(max_length=200, blank=True)
