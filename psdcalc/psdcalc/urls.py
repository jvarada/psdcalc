from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^sign_upload/$', views.SignUpload.as_view(), name="sign_upload"),
    url(r'^finalize_upload/$', views.FinalizeUpload.as_view(), name="finalize_upload"),
    url(r'^compute_psd/$', views.GeneratePSD.as_view(), name="generate_psd"),
    url(r'^get_images/$', views.ImageList.as_view(), name="image_list"),
]
