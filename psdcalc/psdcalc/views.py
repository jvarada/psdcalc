import os
import time
import base64
import hmac
import json
import urllib
import urllib2
import cStringIO
import boto
import random
import numpy as np
from scipy import fftpack
from hashlib import sha1
from PIL import Image

from django.http import JsonResponse
from django.views.generic import View
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from django.conf import settings

from .models import Image as ImageModel


# Home Page View - Renders the main PSDCALC page
@ensure_csrf_cookie
@login_required
def home(request):
    return render(request, template_name="home.html")


# Front-end samples view - Renders the front end samples page
@login_required
def frontend(request):
    return render(request, template_name="frontend.html")


# This view returns the list of images that have been uploaded by the current user,
# when an appropriate asynchronous GET request is performed by the client.
class ImageList(View):

    def get(self, request, *args, **kwargs):

        if request.is_ajax():

            response = []

            images = ImageModel.objects.all()

            for img in images:
                curr_img = {
                    'filename': img.filename,
                    'thumbnail': img.thumbnail,
                    'url': img.url,
                    'psd': img.psd,
                }
                response.append(curr_img)
                print response
            return JsonResponse(json.dumps(response, ensure_ascii=True), safe=False)

        else:

            return JsonResponse({
                'error_msg': 'Requests to this method must be made via AJAX.',
            }, status=400)


    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ImageList, self).dispatch(request, *args, **kwargs)


# This view returns an S3 signature with the proper credentials when a user wants to upload a new
# image, and makes an appropriate GET query.
class SignUpload(View):

    def get(self, request, *args, **kwargs):

        if request.is_ajax():

            AWS_ACCESS_KEY = settings.AWS_ACCESS_KEY
            AWS_SECRET_KEY = settings.AWS_SECRET_KEY
            S3_BUCKET = settings.S3_BUCKET

            original_filename = request.GET['file_name']
            mime_type = request.GET['file_type']

            filename, extension = os.path.splitext(original_filename)

            # Generate a random string of 10 digits to uniquely identify this file
            # in case multiple files with the same name are uploaded
            random.seed()
            random_str = ''.join([str(random.randint(0,9)) for x in range(0,9)])
            amzn_filename = filename + "_" + random_str + extension

            obj_name = urllib.quote_plus(amzn_filename)

            # 5 minutes expiration on the signature
            expires = int(time.time() + 60*5)

            amazon_headers = "x-amz-acl:public-read"

            string_to_sign = "PUT\n\n%s\n%d\n%s\n/%s/%s" % (mime_type, expires, amazon_headers, S3_BUCKET, obj_name)

            signature = base64.encodestring(hmac.new(AWS_SECRET_KEY.encode(), string_to_sign.encode('utf8'), sha1).digest())
            signature = urllib.quote_plus(signature.strip())

            url = 'https://{}.s3.amazonaws.com/{}'.format(S3_BUCKET, obj_name)

            return JsonResponse({
                'signed_request': '{}?AWSAccessKeyId={}&Expires={}&Signature={}'.format(url, AWS_ACCESS_KEY, expires, signature),
                'amzn_filename': amzn_filename,
                'url': url,
            })

        else:

            return JsonResponse({
                'error_msg': 'Requests to this method must be made via AJAX.',
            }, status=400)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(SignUpload, self).dispatch(request, *args, **kwargs)


# This view handles asynchronous POST requests in order to update the metadata about a recently uploaded
# image in the application's database. It also creates a thumbnail of the image for display in the preview
# box of the site.
class FinalizeUpload(View):

    def post(self, request, *args, **kwargs):

        if request.is_ajax():

            user = request.user
            filename, extension = os.path.splitext(request.POST['file_name'])
            amzn_filename, amzn_ext = os.path.splitext(request.POST['amzn_filename'])
            mime_type = request.POST['mime_type']
            url = request.POST['url']

            img_fd = urllib2.urlopen(url)
            img_str = cStringIO.StringIO(img_fd.read())
            img = Image.open(img_str)

            thumbnail_size = (520, 220)
            resized_img = img.resize(thumbnail_size, Image.ANTIALIAS)

            out_img = cStringIO.StringIO()
            resized_img.save(out_img, "JPEG")

            conn = boto.connect_s3(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY)
            bucket = conn.get_bucket(settings.S3_BUCKET)

            thumbnail_filename = amzn_filename + "_thumbnail.jpg"
            key = bucket.new_key(thumbnail_filename)
            key.set_contents_from_string(
                out_img.getvalue(),
                headers={
                    'Content-Type': mime_type,
                    'x-amz-acl': 'public-read',
            })

            out_img.close()
            resized_img.close()
            img.close()
            img_str.close()
            img_fd.close()

            thumbnail_url = 'https://{}.s3.amazonaws.com/{}'.format(settings.S3_BUCKET, key.name)

            ImageModel.objects.create(
                user=user,
                filename=(filename + extension),
                amzn_filename=(amzn_filename + amzn_ext),
                mime_type=mime_type,
                url=url,
                thumbnail=thumbnail_url
            )

            return JsonResponse({
                'thumbnail': thumbnail_url,
            })

        else:

            return JsonResponse({
                'error_msg': 'Requests to this method must be made via AJAX.',
            }, status=400)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(FinalizeUpload, self).dispatch(request, *args, **kwargs)


# This view handles requests to calculate the power spectral density of an image.
class GeneratePSD(View):

    def post(self, request, *args, **kwargs):

        if request.is_ajax():

            user = request.user
            url = request.POST['amzn_url']

            image_entry = ImageModel.objects.get(url=url)

            if image_entry.psd:
                return JsonResponse({ 'psd_url': image_entry.psd })

            amzn_filename, ext = os.path.splitext(image_entry.amzn_filename)

            img_fd = urllib2.urlopen(url)
            img_str = cStringIO.StringIO(img_fd.read())
            img = Image.open(img_str)

            # Convert the image to greyscale for 2D FFT calculation
            gs_img = img.convert('L')

            # Store image in numpy array and calculate 2D FFT, shift it,
            # calculate the power spectral density, normalize it
            arr = np.asarray(gs_img)
            fft = fftpack.fft2(arr)
            shifted_fft = fftpack.fftshift(fft)
            psd = np.abs(shifted_fft)**2
            psd = np.log10(psd)
            norm_psd = (psd - psd.min()) / (psd.max() - psd.min())

            psd_img = Image.fromarray((norm_psd * 255).astype(np.uint8))

            out_img = cStringIO.StringIO()
            psd_img.save(out_img, "JPEG")

            conn = boto.connect_s3(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY)
            bucket = conn.get_bucket(settings.S3_BUCKET)

            psd_filename = amzn_filename + "_psd.jpg"
            key = bucket.new_key(psd_filename)
            key.set_contents_from_string(
                out_img.getvalue(),
                headers={
                    'Content-Type': image_entry.mime_type,
                    'x-amz-acl': 'public-read',
            })

            out_img.close()
            psd_img.close()
            gs_img.close()
            img.close()
            img_str.close()
            img_fd.close()

            psd_url = 'https://{}.s3.amazonaws.com/{}'.format(settings.S3_BUCKET, key.name)

            image_entry.psd = psd_url
            image_entry.save()

            return JsonResponse({ 'psd_url': psd_url })

        else:

            return JsonResponse({
                'error_msg': 'Requests to this method must be made via AJAX.',
            }, status=400)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(GeneratePSD, self).dispatch(request, *args, **kwargs)
