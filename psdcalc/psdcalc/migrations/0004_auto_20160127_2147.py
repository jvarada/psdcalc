# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psdcalc', '0003_auto_20160127_2143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='amzn_filename',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]
