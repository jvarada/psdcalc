# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psdcalc', '0002_auto_20160126_1649'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='extension',
        ),
        migrations.AddField(
            model_name='image',
            name='amzn_filename',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
