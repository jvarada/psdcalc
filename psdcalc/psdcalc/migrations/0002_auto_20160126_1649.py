# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psdcalc', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='image',
            old_name='img_name',
            new_name='filename',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='psd_url',
            new_name='psd',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='img_url',
            new_name='thumbnail',
        ),
        migrations.AddField(
            model_name='image',
            name='extension',
            field=models.CharField(default='', max_length=5),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='mime_type',
            field=models.CharField(default='', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='url',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
